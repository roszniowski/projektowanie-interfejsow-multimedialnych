import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {SpeechRecognitionService} from "@kamiazya/ngx-speech-recognition";
import {TranslateService} from "../translate.service";
import * as MyScriptJS from 'myscript/dist/myscript.min.js';
import {MatChipInputEvent} from "@angular/material";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
export interface Product {
    name: string;
}
@Component({
    selector: 'app-main-component',
    templateUrl: './main-component.component.html',
    styleUrls: ['./main-component.component.scss']
})
export class MainComponentComponent implements AfterViewInit{
    @ViewChild("tref", {read: ElementRef, static: true}) domEditor: ElementRef;
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = true;
    readonly separatorKeysCodes: number[] = [ENTER, COMMA];
    products: Product[] = [
        {name: 'Pomidor'},
        {name: 'Jabłko'},
    ];
    data: any;
    result;
    text = '';
    editor;
    public started = false;
    public message = '';
    constructor(private translateService: TranslateService, public service: SpeechRecognitionService){
        this.service.onresult = (e) => {
            this.message = e.results[0].item(0).transcript;
            this.text = this.message;
        };

    }

    ngAfterViewInit(): void {
        this.editor = MyScriptJS.register(this.domEditor.nativeElement, {
            recognitionParams: {
                type: 'TEXT',
                protocol: 'WEBSOCKET',
                apiVersion: 'V4',
                server: {
                    scheme: 'https',
                    host: 'webdemoapi.myscript.com',
                    applicationKey: '515131ab-35fa-411c-bb4d-3917e00faf60',
                    hmacKey: '54b2ca8a-6752-469d-87dd-553bb450e9ad'
                },
                v4: {
                    export: {
                        jiix: {
                            strokes: true
                        }
                    }
                }
            },
        });
    }

    start() {
        this.started = true;
        this.service.start();
    }

    stop() {
        this.started = false;
        this.service.stop();
    }

    translate(text){
        this.translateService.translate(text).subscribe(
            value => {
                this.data = value;
                this.text = this.data.text[0];
            },
            error => {
                console.log(error);
            }
        );
    }

    add(event: MatChipInputEvent): void {
        const input = event.input;
        const value = event.value;

        if ((value || '').trim()) {
            this.products.push({name: value.trim()});
        }

        if (input) {
            input.value = '';
        }
    }

    remove(product: Product): void {
        const index = this.products.indexOf(product);

        if (index >= 0) {
            this.products.splice(index, 1);
        }
    }

    addToList(product){
        this.products.push({name: product.trim()});
    }
    convert(){
        this.products.push({name: this.editor.smartGuide.previousLabelExport.trim()});
    }
}
