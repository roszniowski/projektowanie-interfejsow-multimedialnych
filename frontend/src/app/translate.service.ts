import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class TranslateService {
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';
    api = 'trnsl.1.1.20200114T192617Z.e7fa08f1464962fe.cf93e77e0bdbc1210108af0088f6ddf93cb1a830';
    result;
    constructor(private http: HttpClient) {
    }

    translate(text){
        return this.http.post(this.url, this.result,{
            params: {
                key: this.api,
                text: text,
                lang: 'en-pl'
            }
        });
    }
}
