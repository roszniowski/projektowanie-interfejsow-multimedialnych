import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainComponentComponent} from './main-component/main-component.component';
import {
    RxSpeechRecognitionService,
    SpeechRecognitionLang,
    SpeechRecognitionModule, SpeechRecognitionService
} from "@kamiazya/ngx-speech-recognition";
import {HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material.module";
import {TranslateService} from "./translate.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        AppComponent,
        MainComponentComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        MaterialModule,
        BrowserAnimationsModule,
        FormsModule,
        SpeechRecognitionModule.withConfig({
            lang: 'en-US',
            interimResults: true,
            maxAlternatives: 10,
        }),
        ReactiveFormsModule,
    ],
    providers: [
        RxSpeechRecognitionService,
        SpeechRecognitionService,
        {provide: SpeechRecognitionLang, useValue: 'en-US'},
        TranslateService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
